Review for assignment set 1 for group J (S3683311)

Note: This group only consists of one student.

Exercise 1
The creation of the tar sourceball works.
The cabal file contains all necessery information and the README is automatically copied in the tar file.
The README files explains what was done in this package and explains how to use it -> very good.
Cabal build works.
Installation of the package worked.
Grade: 1

Exercise 2.5
Works perfectly and the comments explain the implementation very well.
Grade: 1

Exercise 7.1
The code is very clean and the single functions are short and easy to understand.
Additionally, the code is documented very well.
The code compiles and works.
Grade: 1

Exercise 8.1
The code is well documented as well. There are extensive benchmark results as well as a conclusion.
There are 3 heap profiles for the new implementation as well as 2 for the given naive implementation.
Additionally, the heap profiles are analyzed and the conditions of the profiling are explained. Very good :)
Grade: 1

Exercise 9.1
The proof is correct and good to understand because for every step there's a note which explains what has been done.
Grade: 1

Grades:
1	1	1	1	1