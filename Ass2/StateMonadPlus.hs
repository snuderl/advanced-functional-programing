{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module StateMonadPlus (StateS, StateMonadPlus, diagnostics, annotate, runStateMonadPlus, main) where

import Data.List
import Control.Monad.State

-- |This extended state is a triple. The first value is the state,
-- the second the list of executed methods (e.g return)
-- and the third is the stack of states. The stack enables to save and load states.
type StateS s = (s, [String], [s])

-- |This is the extended State data type.
data StateMonadPlus s a = StateMonadPlus { runStatePlus :: Either String (StateS s) -> Either String (a, StateS s) }

-- |This method adds a given value to the second value of a triple.
-- Thereby, the second parameter of the triple is a list of the given value.
add :: a -> (b, [a], c) -> (b, [a], c)
add val (s, c, stack) = (s, val : c, stack)

-- |StateMonadPlus is made a instance of Monad by implementing 'return' and '>>='.
-- Additionally, 'fail' is implemented in order to allow controlled failure of the StateMondPlus' computations.
instance Monad (StateMonadPlus s) where
	return x = StateMonadPlus $ \st -> 
		st >>= \s -> Right (x, add "return" s)
	(StateMonadPlus h) >>= k = StateMonadPlus $ \st-> 
		st  >>= \s -> h (Right (add "bind" s))
			>>= \(a, s') -> 
				let (StateMonadPlus g) = k a
						in g $ Right s'
	fail s = StateMonadPlus $ \st -> Left (s ++ " fail")

-- |This method takes no parameters and returns a StateMonadPlus
-- which contains a String as the current value that describes which methods were executed how often on the StateMonadPlus.
diagnostics :: StateMonadPlus s String
diagnostics = StateMonadPlus (\st -> 
	st >>= \s ->	let ext@(_, c, _) = add "diagnostics" s in
		Right (statistics c, ext))
			where statistics xs = intercalate "," (map (\x -> (head x) ++ "=" ++ show (length x)) $ group $ sort xs)

-- |This method allows to annotate a computation of a given StateMonadPlus with a String.
annotate :: String -> StateMonadPlus s a -> StateMonadPlus s a
annotate a (StateMonadPlus f) = StateMonadPlus $ \st ->
		f st >>= \(a', s') -> Right (a', add a s')
				

start :: t -> (t, [a], [t])
start state = (state, [], [])

-- |This method runs a StateMonadPlus. It returns Left <<message>> if the computation fails.
-- Otherwise it returns Right (<<value>>, <<state>>)
runStateMonadPlus :: StateMonadPlus s a -> s -> Either String (a, s)
runStateMonadPlus k state = f $ runStatePlus k $ Right (start state)
	where 
		f (Left s) = Left s
		f (Right (a, (b, _, _))) = Right (a, b)


test :: StateMonadPlus s String
test = do 
	annotate "A" (return (5 :: Integer) >> return (5 :: Integer))
	return (5 :: Integer)
	diagnostics

run x = runStateMonadPlus x ()

-- |StateMonadPlus is made a instance of MonadState.
-- Therefor the get and put methods are implemented.
-- 'get' puts the state as the current value and returns the tuple encapsulated in a Right (Either).
-- 'put' sets the state to its parameter s and returns all in a Right (Either).
-- Thereby the current value is empty.
instance MonadState s (StateMonadPlus s) where
    get   = StateMonadPlus $ \st ->
    	st >>= \(s, c, stack) -> Right (s, (s, c, stack))
    put s = StateMonadPlus $ \st ->
    	st >>= \(_, c, stack) -> Right ((), (s, c, stack))

-- |This is the given typeclass for StoreState,
-- which defines the two methods 'saveState' and 'loadState' to save/load a state from the stack.
class MonadState s m => StoreState s m | m -> s where
	saveState :: m ()
	loadState :: m ()

-- |StateMonadPlus is made a StoreState instance.
-- This allows to save a state on the stack or load it from there.
instance StoreState s (StateMonadPlus s) where
	saveState = StateMonadPlus $ \st ->
		st >>= \(s, c, stack) -> Right ((), (s, c, s:stack))
	loadState = StateMonadPlus $ \st ->
		st >>= \(s, c, stack) -> case stack of
			(x:xs) -> Right ((), (x, c, xs))
			[] -> Left "Stack is empty"

-- |This method tests the behaviour defined by StoreState for StateMonadPlus.
stackTest = do
	i1 <- get; saveState
	modify (*2)
	i2 <- get; saveState
	modify (*2)
	i3 <- get
	loadState
	i4 <- get
	loadState
	i5 <- get
	return (i1, i2, i3, i4, i5)

main :: IO ()
main = do
	print $ run (return 1 >> return 3 >> (annotate "abc" $ return 3)  >> diagnostics)
	print $ run (return 1 >> return 3 >> fail "epic" >> (annotate "abc" $ return 3)  >> diagnostics)
	print $ (runStateMonadPlus stackTest 1)





