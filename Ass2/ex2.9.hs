-- In the first implementation we used list to hold the stack. 
-- With list however there is no way for the type checker to figure out
-- if the program is correct. We switched the stack to tuples, this way we can 
-- check the correctnes on the final call top 'stop'. It should contain a tuple with type
-- (Int, ((), ())). First Int is the result and units are just placeholder values passed 
-- on by start (we cant have empty tuple...)

store :: b -> a -> ((a, b) -> t) -> t
store xs x = \k -> k (x, xs)

start :: (((), ()) -> t) -> t
start = \k -> k ((), ())

mul :: Num a => (a, (a, k)) -> ((a, k) -> t) -> t
mul (x, (y, xs)) = \k -> k (x * y, xs)

add :: Num a =>  (a, (a, k)) -> ((a, k) -> t) -> t
add (x, (y, xs)) = \k -> k (x + y, xs)

stop :: (Int, ((), ())) -> Int
stop (a, _) = a


p1 :: Int
p1 = start store 3 store 5 add stop
p2 = start store 3 store 6 store 2 mul add stop
-- p3 = start store 4 add stop


main = print p1
	>> print p2

--p2 = start store 3 store 6 store 2 mul add stop
--p3 = start store 2 add stop



