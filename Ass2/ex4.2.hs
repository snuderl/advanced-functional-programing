-- the given data structures to define square matrices.
type Square a = Square' Nil a
data Square' t a = Zero (t (t a)) | Succ (Square' (Cons t) a)
data Nil a = Nil
data Cons t a = Cons a (t a)

--random trials, not part of the solution
n = Nil
k = Cons (Cons 1 Nil) Nil
k2 = Cons (Cons 1 (Cons 2 Nil)) Nil
z = Zero k
z1 = Zero n
m = Succ z


-- |2x2 matrice
-- In general the number of Succ is the dimension of the matrice.
-- Zero contains the actual columns of the matrice.
m1 :: Square Int
m1 = Succ (
                Succ (
                        Zero (
                                Cons (
                                	-- column 1
	                                    Cons
		                                    	1
		                                    	(Cons 0 Nil)
                                )
                                	-- column 2
                                	(Cons (
	                                		Cons
		                                			0
		                                			(Cons 1 Nil)
	                                )
	                                		Nil
                                )
                        )
                )
          )

-- 3x3 matrice
m2 :: Square Int
m2 = Succ (
		Succ (
			Succ (
				Zero (
					Cons (
						-- column 1
						Cons
							1
							(
							Cons
								4
								(Cons 7 Nil)
						)
						
					)
						(
						Cons (
							-- column 2
							Cons 
								2
								(
								Cons 
									5
									(Cons 8 Nil)
							)
						)
							(
							Cons (
								-- column 3
								Cons
									3
									(
									Cons
										6
										(Cons 9 Nil)
								)
							)
								Nil
						)
					)
				) 
			)
		)
	)