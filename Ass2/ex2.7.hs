{-# LANGUAGE FlexibleContexts #-}
import Control.Monad
import Control.Monad.State
import Control.Monad.Identity
import Control.Monad.Writer


type Object a = a -> a -> a
data X = X {n :: Int, f :: Int -> Int}
x, y, z :: Object X
x super this = X {n = 0, f = \i -> i + (n this)}
y super this = super {n = 1}
z super this = super {f = f super . f super}

extendedBy :: Object a -> Object a -> Object a
extendedBy o1 o2 super this = o2 (o1 super this) this

fixObject o = o (error "super") (fixObject o)


fac :: Monad m => Object (Int -> m Int)
fac super this n =
	case n of
		0 -> return 1
		n -> liftM (n*) (this (n - 1))

calls :: MonadState Int m => Object (a -> m b)
calls super this n = do
	modify (+1)
	super n


trace super this n = do
		enter n
		val <- super n
		exit n
		return val
	where 		
		enter x = writer (x, [Enter x])
		exit x = writer (x, [Return x])



data Step a b = Enter a
		| Return b
	deriving Show


-- | This is very subtle. If we extend zero by x, we get 
-- x (zero super this) this -> x super this, which is just x.
-- On the other hand if we do x extendBy zero we get 
-- zero (x super this) this -> x super this      (zero just passes on the second parameter)
zero :: Object t
zero super this = super

zeroTest = (print $ n $ fixObject $ x `extendedBy` zero)
	>> (print $ n $ fixObject $ x `extendedBy` y `extendedBy` zero)
	>> (print $ n $ fixObject $ x `extendedBy` zero `extendedBy` y)
	>> (print $ n $ fixObject $ zero `extendedBy` x)
	>> (print $ n $ fixObject $ zero `extendedBy` x `extendedBy` y `extendedBy` zero)

main = zeroTest
	>> print (runIdentity (fixObject fac 5))
	>> print (runState (fixObject (fac `extendedBy` calls) 5) 0)
	>> print (runWriter (fixObject (fac `extendedBy` trace) 5))





