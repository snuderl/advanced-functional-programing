--import Control.Monad.State
import Data.List
import Control.Arrow

data StateMonadPlus s a = StateMonadPlus {runStatePlus :: s -> [String] -> (a, s, [String])}

instance Monad (StateMonadPlus s) where
    return a = StateMonadPlus $ \s diag -> (a, s, "return":diag)
    processor >>= processorGen = StateMonadPlus $ \s diag ->
                                 let (a, s', diag') = runStatePlus processor s diag
                                 in runStatePlus (processorGen a) s' ("bind":diag')

diagnostics :: StateMonadPlus s String
diagnostics = StateMonadPlus $ \s diag -> let
                                          xs = map (head &&& length) $ group $ sort ("diagnostics":diag) --get a list of tuples. The first part of each tuple contains the function and the second one the count.
                                          xs' = map (\(func, count) -> func ++ "=" ++ show count) xs --transform each tuple to a String of the form "function=count"
                                          a = "[" ++  (concat $ intersperse "," xs') ++ "]" --create final output
                                          in (a, s, "diagnostics":diag)

annotate :: String -> StateMonadPlus s a -> StateMonadPlus s a
annotate str (StateMonadPlus f) = StateMonadPlus $ \state diag ->
                                    let (a', s', diag') = f state diag
                                    in (a', s', str:diag')


runStateMonadPlus :: StateMonadPlus s a -> s -> (a,s)
runStateMonadPlus x state = f  $ runStatePlus x state []
    where f (a, b, _) = (a, b)

third (a,b,c) = c

run x = runStateMonadPlus x ()

main = print $ fst $ run (annotate "A" (return 3 >> return 5) >> diagnostics)