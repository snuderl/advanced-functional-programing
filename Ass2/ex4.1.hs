
-- y = \f -> (\x -> f (x x)) (\x -> f (x x))



data F a = F {unF :: F a -> a}

y f = t1 t2
	where 
		t1 x = x (F x)
		t2 x = f $ (unF x) x

fac _ 0 = 1
fac f n = n * (f (n - 1)) 

main = print $ y fac 5