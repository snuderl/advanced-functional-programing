--module ChatServer where

import Control.Concurrent
import Control.Concurrent.STM
import Control.Exception
import Control.Monad
import Control.Monad.Fix (fix)
import Network.Socket
import System.IO

--			     id, message
type Message = (Int, String)

-- |The mainloop which constantly waits for new clients and accepts them if they try to connect.
-- Every new client runs in an own thread.
mainLoop :: Socket -> TChan Message -> Int -> IO ()
mainLoop socket tchan id = do
	conn <- accept socket
	forkIO (runClient conn tchan id)
	mainLoop socket tchan (id+1)

-- |This method runs a client.
-- It adds messages from the client to the TChan and sends him messages from other users
-- as well as announcements from the server.
runClient :: (Socket, SockAddr) -> TChan Message -> Int -> IO ()
runClient (sock, sockAddr) tchan id = do
	socketHandle <- socketToHandle sock ReadWriteMode
	-- wait for client to send its name
	--hWaitForInput socketHandle (-1)
	clientName <- hGetLine socketHandle
	putStrLn $ "Client " ++ clientName ++ " connected."
	--helper method to broadcast messages
	let broadcast line = atomically $ writeTChan tchan (id, line)
	--duplicate channel
	tchan' <- atomically $ dupTChan tchan
	-- tell other users that new client joined the server
	broadcast $ ">>>> " ++ clientName ++ " joined the server."
	-- fork channel reader
	channelReader <- forkIO $ fix $ \loop -> do
		(id', msg) <- atomically $ readTChan tchan'
		when (id /= id') $ hPutStrLn socketHandle (msg)
		loop
	
	-- read incoming messages from the socket and broadcast them to the clients.
	handle (\(SomeException _) -> return ()) $ fix $ \loop -> do
		line <- hGetLine socketHandle
		case line of
			":quit" -> do return ()
			otherwise -> do
				broadcast $ "<" ++ clientName ++ "> " ++ line
				loop
	killThread channelReader
	hClose socketHandle
	broadcast $ "<<<< " ++ clientName ++ " left the server."
	putStrLn $ "Client " ++ clientName ++ " disconnected."


-- |main method. Creates socket and communication channel.
-- Afterwards, the main loop is started.
main :: IO ()
main = do
	-- create the socket
	sock <- socket AF_INET Stream 0
	-- make socket immediately reusable - eases debugging.
	setSocketOption sock ReuseAddr 1
	-- listen on TCP port 9595
	bind sock (SockAddrInet 9595 iNADDR_ANY)
	-- allow a maximum of 3 outstanding connections
	listen sock 3
	-- initiate communication channel
	tchan <- newTChanIO
	mainLoop sock tchan 1