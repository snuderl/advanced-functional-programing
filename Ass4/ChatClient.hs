--module ChatClient where

import Control.Concurrent
import Control.Concurrent.STM
import Control.Exception
import Control.Monad
import Control.Monad.Fix (fix)
import Network.Socket
import System.Environment
import System.IO

-- |programm needs two arguments:
--  first argument:  hostaddress of the chat server in the usual format: e.g 1.2.3.4
--  second argument: nickname
--  Note: Program might crash with unsuitable arguments
main :: IO ()
main = do
	args <- getArgs
	--create the socket
	sock <- socket AF_INET Stream 0

	-- make socket immediately reusable - eases debugging.
	setSocketOption sock ReuseAddr 1
	addr <- inet_addr $ head args
	let nickname = args !! 1
	-- bind socket to port and host address
	connect sock $ SockAddrInet 9595 addr
	socketHandle <- socketToHandle sock ReadWriteMode

	-- send nickname to the server
	hPutStrLn socketHandle nickname
	
	-- loop to read user input from the command line
	forkIO $ fix $ \loop -> do
		line <- getLine
		when (length line > 0) $ hPutStrLn socketHandle line
		loop
	
	-- thread to get new messages from the server and print them
	handle (\(SomeException _) -> return ()) $ fix $ \loop -> do
		line <- hGetLine socketHandle
		putStrLn line
		loop
	putStrLn "You left the server."
	

	