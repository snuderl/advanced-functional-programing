{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE OverlappingInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE DeriveGeneric #-}



import GHC.Generics
import Test.QuickCheck
import Control.Applicative
import Data.List

data Type = N | Rec
class GShow' f where
	gshow' :: Bool -> Type -> f a -> String	

instance GShow' U1 where 
	gshow' top t U1 = ""

instance (GShow' a, GShow' b) => GShow' (a :+: b) where
	gshow' top t (L1 x) = gshow' top t x
	gshow' top t (R1 x) = gshow' top t x

instance (GShow a) => GShow' (K1 i a) where
	gshow' top t (K1 x) = gshowDef top x



instance (Selector c, GShow' a) => GShow' (M1 S c a) where
	gshow' top t c@(M1 x) = case selName c of
		""        -> gshow' top t x 
		otherwise -> selName c ++ " = " ++ gshow' top t x			


instance (GShow' a, Constructor c) => GShow' (M1 C c a) where
	gshow' top t c@(M1 x) = if top then body else "(" ++ body ++ ")"
		where 
		  body = case conIsRecord c of 
		  	True ->  conName c ++ " {" ++ gshow' True Rec x ++ "}"
		  	False -> conName c ++ " " ++ gshow' False N x

instance (GShow' a) => GShow' (M1 D c a) where
	gshow' top t c@(M1 x) = gshow' top t x

instance (GShow' a, GShow' b) => GShow' (a :*: b) where
	gshow' top Rec (x :*: y) = gshow' top Rec x ++ ", " ++ gshow' top Rec y
	gshow' top t (x :*: y) = gshow' False t x ++ " " ++ gshow' False t y



class GShow a where
	gshow :: a -> String
	gshow = gshowDef True
	gshowDef :: Bool -> a -> String
	default gshowDef :: (Generic a, GShow' (Rep a)) => Bool -> a -> String
	gshowDef top = gshow' top N . from

instance GShow Int  where gshowDef = showNum
instance GShow String  where gshowDef t = show
instance GShow Char  where gshowDef t = show
instance GShow Float  where gshowDef = showNum

showNum :: (Ord a, Num a, Show a) => Bool -> a -> String
showNum True x = show x
showNum False x = if x < 0 then "(" ++ show x ++ ")" else show x


instance (GShow a) => GShow [a] where
  gshowDef top l =  "[" ++ concat (intersperse "," (map (gshowDef True) l)) ++ "]"

instance (GShow a, GShow b) => GShow (a,b) where
  gshowDef top (x,y) =    "(" ++ gshowDef True x ++ "," ++ gshowDef True y ++ ")"


data Tree a = Node (Tree a) (Tree a) | Leaf a
	deriving (Show, Read, Generic)
data Foo = Foo {bar :: String, tar :: Int, k :: Tree Int} deriving (Show, Read, Generic)

instance (GShow a) => GShow (Tree a)
instance GShow Foo

testShowString :: String -> Bool
testShowString a = show a == gshow  a

testShowTuple :: (Int, Float) -> Bool
testShowTuple a = show a == gshow a


testShowList :: [Int] -> Bool
testShowList a = show a == gshow a

testShowTree :: Tree Float -> Bool
testShowTree a = show a == gshow a

testShowFoo :: Foo -> Bool
testShowFoo a = show a == gshow a



instance Arbitrary a => Arbitrary (Tree a) where
	arbitrary = frequency 
		[ (3, Leaf <$> arbitrary)                   -- The 3-to-1 ratio is chosen, ah,
												  -- arbitrarily...
												  -- you'll want to tune it
		, (1, Node <$> arbitrary <*> arbitrary)]

instance Arbitrary Foo where
	arbitrary = do
		s <- arbitrary
		l <- arbitrary
		t <- arbitrary
		return Foo{tar=s,bar=l,k=t}

main = do
	quickCheck testShowString
	quickCheck testShowTree  -- Recursive types
	quickCheck testShowTuple  
	quickCheck testShowList  --- Lists
	quickCheck testShowFoo   --- Record syntax
