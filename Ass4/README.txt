This is Assignment Set 4
Students:
Lucas K�hler, F132946, l.koehler93@gmail.com
Blaz Snuderl, F133788, snuderl@gmail.com

The solutions to exercise 11.1 and 11.2 are in the haskell files with the corresponding file names.
The chat server and chat client should be compiled with ghc to use them.
To start the chat client correctly it needs two parameters.
The first parameter is the host address of the chat server in the usual format (e.g. 127.0.0.1).
The second parameter is the nickname of the user.