{-# LANGUAGE DeriveDataTypeable #-}

import Data.Typeable
import Data.Data
import Test.QuickCheck
import Data.Generics.Text
import Data.Generics.Aliases
import Data.List
import Control.Applicative
import Control.Monad.State


show' :: Data a => a -> String
show' =  (show'' True)
show'' :: Data a => Bool -> a -> String
show'' top = handleTuple inner			
		`extQ` (showBase :: String -> String)
		`extQ` (showNum :: Int -> String)
		`extQ` (showBase :: Float -> String)

	where 
		inner = (\x -> case length $ gmapQ (show'' top) x of
			0 -> (showConstr $  toConstr x)
			otherwise -> showFixity (constrFixity $ toConstr x) x top) 


handleTuple :: Data a => (a -> String) -> a -> String
handleTuple f x = 
	case showConstr (toConstr x) of
		"[]" -> "]"
		"(:)" -> (f x)
		"(,)" -> "(" ++ concat (intersperse "," (gmapQ (show'' False) x)) ++ ")"
		otherwise -> f x

showFixity Prefix x top = case top of
		True -> body
		False -> "(" ++ body ++ ")"
	where 
		c = toConstr x
		values = gmapQ (show'' False) x
		fields = constrFields c
		body = showConstr c
			++ case fields of
				[] -> concat (map (" " ++) values)
				otherwise -> 
					" {" 
					++ concat(
						intersperse ", " 
						(map (\(a,b) -> a ++ " = " ++ b) 
							(zip fields values)))
					++ "}"
showFixity Infix  x _ = 
	"("
	++ head arguments 
	++ (showConstr $ toConstr x) 
	++ (concat (tail arguments)) 
	++ ")"
	where arguments = gmapQ (show'' False) x 

showBase x = show x
showNum :: (Show a, Num a, Ord a) => a -> String
showNum x = if x < 0 then "(" ++ show x ++ ")" else show x

cleanup :: String -> String
cleanup (' ':'(':xs) = init xs
cleanup ('(':xs) = init xs
cleanup xs = xs

trim = unwords . words


trimSpaces :: State String String
trimSpaces = do
	x <- get
	case x of
		(' ': xs) -> do
			put xs
			trimSpaces
		otherwise -> return ""

readChar :: Char -> State String String
readChar c = do
	x <- get
	case x of
		(y : xs) -> if y == c then (do put xs >> return [y]) else return ""
		otherwise -> error "Error empty string"

readAny :: State String String
readAny = do
	x <- get
	case x of 
		(y:ys) -> do
			put ys
			return [y]
		[] -> return ""

delims = ["(", ")", " ", ",", "}", "{"]


readNextWord :: String -> State String String
readNextWord acc = do
	x <- get
	processChar x
	where 
		processChar [] = return acc
		processChar (x:xs) 
			| elem [x] delims = do
				return acc
			| otherwise = do
				put xs
				readNextWord (acc++[x])

data Simple = Simple deriving (Show, Typeable, Data)

read' :: Data a => String -> a
read' x = evalState read'' x
read'' :: Data a => State String a
read'' = readInner
	where
		myDataType = dataTypeOf (getArg readInner) -- A trick to get the datatype
			where
      			getArg :: State String a -> a
      			getArg = undefined
		readInner = do
			trimSpaces
			b <- readChar '('
			trimSpaces
			const <- readNextWord ""
			case readConstr (myDataType) const of
				Nothing -> do
					-- Todo: Handle infix constructors
					s' <- get
					error ("Cannot create constructor out of '" ++ const ++ " , for datatype " ++ show myDataType)	
				Just c -> do
					trimSpaces
					case isAlgType $ constrType c of
						True -> do
							let fields = constrFields c
							case fields of 
								--- Normal data types
								[] -> do
									val <- fromConstrM read'' c
									trimSpaces
									if b == "(" then readChar ')' else return ""
									return val
								--- Record syntax
								otherwise -> do 
									readChar '{'
									val <- fromConstrM readRecordSyntax c
									trimSpaces
									readChar '}'
									if b == "(" then readChar ')' else return ""
									return val
			 -- This are built in types that we can't use the 
			 -- constrFields method on as it throws exception
						False -> do                              
							let val = fromConstr c
							if b == "(" then readChar ')' else return ""
							return val


--- Discard input until we get to '=' then go to read''
readRecordSyntax :: Data a => State String a
readRecordSyntax = do
	w <- readAny
	if w == "=" then 
		do 
			r <- read''
			s <- get
			case s of 
				(',':xs) -> do readAny; return r
				otherwise -> do return r
	else readRecordSyntax



testShowString :: String -> Bool
testShowString a = show a == show' a

testShowTuple :: (Int, Int) -> Bool
testShowTuple a = show a == show' a


testShowList :: [Int] -> Bool
testShowList a = show a == show' a

--- To custom data types to test our implementation on. Tree is a recursive data type.
--- And Foo uses record syntax.
data Tree a = Node (Tree a) (Tree a) | Leaf a
	deriving (Show, Read, Data, Typeable, Eq)

data Foo = Foo {bar :: Float, tar :: Float} deriving (Show, Read, Data, Typeable, Eq)

testShowTree :: Tree Int -> Bool
testShowTree a = show a == show' a

testShowFoo :: Foo -> Bool
testShowFoo a = show a == show' a




testReadInt :: Int -> Bool
testReadInt a = a == (read' $ show a :: Int)

testReadTree :: Tree Int -> Bool
testReadTree a = a == (read' $ show a :: Tree Int)

testReadString :: String -> Bool
testReadString a = a == (read' $ show a :: String)


testReadFoo :: Foo -> Bool
testReadFoo a = a == (read' $ show a :: Foo)



testShowReadInt :: Int -> Bool
testShowReadInt a = (read' (show' a) :: Int) == a

testShowReadTree :: Tree Int -> Bool
testShowReadTree a = (read' (show' a) :: Tree Int) == a


testShowReadFoo :: Foo -> Bool
testShowReadFoo a = (read' (show' a) :: Foo) == a


instance (Arbitrary a) => Arbitrary (Tree a) where
	arbitrary = frequency 
		[ (3, Leaf <$> arbitrary)                   -- The 3-to-1 ratio is chosen, ah,
		                                          -- arbitrarily...
		                                          -- you'll want to tune it
		, (1, Node <$> arbitrary <*> arbitrary)]

instance Arbitrary Foo where
	arbitrary = do
		s <- arbitrary
		l <- arbitrary
		return Foo{tar=s,bar=l}

--- We have implemented reading and parsing of some basic built in types such as string,
--- int, float and tuples. Building on that we support bolt recursive data types and
--- record syntax.


main = do
	--- This tests check equvivalence with built-in show method
	quickCheck testShowString
	quickCheck testShowTree
	quickCheck testShowFoo
	--- This tests check equvivalence with built-in read method
	quickCheck testReadInt 
	quickCheck testReadTree
	quickCheck testReadFoo
	--- This tests check whether runing read' on the output of show' produces original value.
	quickCheck testShowReadInt
	quickCheck testShowReadTree
	quickCheck testShowReadFoo

