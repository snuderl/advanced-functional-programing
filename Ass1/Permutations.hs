

module Permutations 
	( smoothPerms
	, smoothPermsNaive
	) where

import Data.List

split [] = []
split (x:xs) = (x, xs):[(y, x:ys) | (y, ys) <- split xs]

perms [] = [[]]
perms xs = [(v:p) | (v, vs) <- split xs, p <- perms vs]

smooth n (x:y:ys) = abs (y - x) <= n && smooth n (y:ys)
smooth _ _ = True

-- |This function generates all the smooth permuations of a list. It does so in a naive way,
-- by creating a list of all permutations and filtering it.
smoothPermsNaive :: Int -> [Int] -> [[Int]]
smoothPermsNaive n xs = filter (smooth n) (perms xs)

data Tree a = Node [(a, Tree a)] | Leaf
	deriving Show

toList :: (Tree a) -> [[a]]
toList (Node ((x, Leaf):xs)) = [x] : toList (Node xs)
toList (Node ((x, tree):xs)) = map (x:) (toList tree) ++ toList (Node xs)
toList _ = []

generateTree :: [Int] -> Tree Int
generateTree [] = Leaf
generateTree xs = Node [(v, generateTree vs) | (v, vs) <- split xs]

pruneTree _ Leaf = Leaf
pruneTree n (Node xs) = Node (map cond xs)
	where cond (val, node) = (val, pruneNode n val node)


pruneNode _ _ Leaf = Leaf
pruneNode n a (Node xs) = pruneTree n (Node $ filter cond xs)
	where 
		cond (x, _) = abs(a - x) <= n

-- |This function generates all the smooth permuations of a list. It does so by representing the permutations using
-- a tree structure, and pruning the nodes as high in the tree as possible, thus avoiding the generation
-- of a high number of non-smooth permutations.
smoothPerms _ [] = [[]]
smoothPerms n xs = toList $ pruneTree n $ generateTree xs