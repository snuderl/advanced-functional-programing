﻿AFP - Assignment 1 README
Students:
Lucas Köhler, F132946, l.koehler93@gmail.com
Blaz Snuderl, F133788


Most of the stuff can be done using the makefile.
Test can be run using
	make test
Profiling can be invoked using
	make profile
Haddock documentation can be generated using
	make doc

Profiling the code shows some interesting results. The total number of permutations quickly explodes,
as there are n! total permutations for a list of length n. However when we use a strong smoothing parameter,
the total number of permutations shrink considerably and this makes the tree version much faster on larger lists. We have run the heap profiler on a naive solution with smoothing of 2 and a list of size 11. It took 
30 seconds to compute all the permutations. On the other hand a list of size 15 with the optimized solutions took less than 0.2 seconds. If we however lower the smoothing constraints timings become more and more alike and at some ponint naive solutions starts to perform better. 
Both solutions seem to use a more or less constant amount of memory, atleast as far as we were able to observe from the relatively small range of (acceptable) input values.
