﻿module TypeHidingCount where

test :: [Int]
test = [count,count 1 2 3,count "" [True,False] id (+)]

-- |This typeclass CountType is designed to implement a 'count' function
-- that accepts an arbitrary number of arguments of arbitrary types.
-- The variable number of arguments is enabled because the 'count'' function
-- has the CountType again as its return type.
class CountType c where
	count' :: Int -> c

-- |This instance of CountType is the //basecase.
-- Its 'count'' function just returns the given Int.
instance CountType Int where
	count' x = x

-- |This instance of CountType has a function as its type
-- whose return type is CountType again.
-- Therefore 'count'' returns a function that accepts an arbitrary argument.
-- This enables an arbitrary amount of arguments of arbitrary types for the 'count'' and therefore the 'count' function.
instance (CountType c) => CountType (a -> c) where
	count' x = \y -> count' (x+1)

-- |The 'count' function returns the number of its arguments.
-- It thereby takes an arbitrary number of arguments.
-- The arguments can be of arbitrary types. 
count :: (CountType c) => c
count = count' 0