import Criterion.Main
import Permutations


smoothTree = uncurry smoothPerms
smoothNaive = uncurry smoothPermsNaive

main = defaultMain [
	  bgroup "Permutations" [ bench "tree1"  $ nf  smoothTree (1, [1..9])
	  			   , bench "tree2"  $ nf  smoothTree (2, [1..9])
	               , bench "naive2" $ nf  smoothNaive (2, [1..9])
	               , bench "tree3"  $ nf  smoothTree (3, [1..8])
	               , bench "naive3" $ nf  smoothNaive (3, [1..8])
	               , bench "tree4"  $ nf  smoothTree (4, [1..7])
	               , bench "naive4" $ nf  smoothNaive (4, [1..7])
	               ]
	  ]