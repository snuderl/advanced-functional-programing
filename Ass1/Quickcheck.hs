import Test.QuickCheck
import Permutations

allSmoothPerms x xs = smoothPerms x xs == smoothPermsNaive x xs

main = do
	quickCheckWith stdArgs { maxSize = 9 } allSmoothPerms

