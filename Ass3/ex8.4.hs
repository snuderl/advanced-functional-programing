--- We know that pattern matching forces the evaluation of an element.
--- The idea was to make a recursive function that returns some results at the end.
--- This way the forceBoolList function always has to traverse the whole list.
--- Actualy forcing the values is done at each step using the pattern matching, about whose 
--- results we really dont care.

forceBoolList :: [Bool] -> r -> r
forceBoolList (True:xs) x = forceBoolList xs x
forceBoolList (False:xs) x = forceBoolList xs x
forceBoolList [] x  = x


sums 0 = 0
sums n = n + sums ((n - 1))


--- By increasing the value in expresion (sums n), we see that execution time is starting to increse,
--- meaning that expression is being evaluated to a boolean
main = print $ forceBoolList [True, False, (sums 10000000) > 500] 5

