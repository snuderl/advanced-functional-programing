 {-# LANGUAGE RankNTypes #-}
 {-# LANGUAGE TypeSynonymInstances, FlexibleInstances #-}


module Test where

-- the given data structures to define square matrices.
type Square a = Square' Nil a
data Square' t a = Zero (t (t a)) | Succ (Square' (Cons t) a)
data Nil a = Nil
data Cons t a = Cons a (t a)

fNil :: (a -> b) -> (Nil a -> Nil b)
fNil f Nil = Nil

fCons :: (forall a b. (a -> b) -> (t a -> t b)) -> (a -> b) -> (Cons t a  -> Cons t b)
fCons fT fA (Cons x xs) = Cons (fA x) (fT fA xs) 

fSquare' :: (forall a b. (a -> b) -> (t a -> t b)) -> (a -> b) -> (Square' t a -> Square' t b)
fSquare' fT fA (Zero xs) = Zero (fT (fT fA) xs)
fSquare' fT fA (Succ xs) = Succ $ fSquare' (fCons fT) fA xs

mapSquare = fSquare' fNil

instance Functor (Square' Nil)  where
 	fmap = mapSquare

m1 :: Square Int
m1 = Succ (
                Succ (
                        Zero (
                                Cons (
                                	-- column 1
	                                    Cons
		                                    	1
		                                    	(Cons 0 Nil)
                                )
                                	-- column 2
                                	(Cons (
	                                		Cons
		                                			0
		                                			(Cons 1 Nil)
	                                )
	                                		Nil
                                )
                        )
                )
          )


-- ow look, it works
run :: (a -> b) -> Square a -> Square b
run f = fmap f