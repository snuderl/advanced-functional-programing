{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}

import Control.Monad.Reader
import System.Random

one :: Int
one = 1
two :: Int
two = 2


-- After figuring out the correct types (show in comments) the evidence translation was pretty straighforward.
-- For every constraint we need to pass in a dictionary with the required functions. Function definitions get 
-- a little out of control.

-- randomN :: (RandomGen g) => Int -> g -> Int
randomN :: DictRandomGen g -> Int -> g -> Int
randomN dictR n g = (fst (next1 dictR $ g) `mod` (two * n + one)) - n

-- sizedInt :: (RandomGen g, MonadReader g m, MonadReader Int (t m), MonadTrans t) => t m Int
sizedInt :: 
	DictRandomGen g -> DictMonadReader g m -> DictMonadReader Int (t m) -> 
	DictMonadTrans t -> DictMonad m -> DictMonad (t m) ->
	t m Int
sizedInt dictR dictMR1 dictMR2 dictMT dictM1 dictM2 = do
	(bind dictM2) (ask1 dictMR2)  f1
	where 
		f1 n = (bind dictM2) ((lift1 dictMT dictM1)  (ask1 dictMR1))  (f2 n)
		f2 n g = (return1 dictM2)(randomN dictR n g)


data DictRandomGen g = DictRandomGen {
	next1 :: g -> (Int, g)
}

data DictMonadReader r m = DictMonadReader {
	ask1 :: m r
}

data DictMonad m = DictMonad {
	bind  :: forall a b. m a -> (a -> m b) -> m b,
	return1 :: forall a. a -> m a
}

data DictMonadTrans t = DictMonadTrans{
	lift1 :: forall m a. DictMonad m -> m a -> t m a
}



