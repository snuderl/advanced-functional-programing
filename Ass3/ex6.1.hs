{-# LANGUAGE GADTs #-}

data Contract a where
	Pred  :: (a -> Bool) -> Contract a
	DFun  :: Contract a -> (a -> Contract b) -> Contract (a -> b)
	Index ::  Contract ([a] -> Int -> a)
	List :: Contract a -> Contract [a]


assert :: Contract a -> a -> a
assert (Pred p)        x  = if p x then x else error "contract violation"
assert (DFun pre post) f  = \arg -> assert (post arg) (f (assert pre arg))
assert Index      	   f  = \xs ind-> if length xs > ind then f xs ind else error "index contract violation"
assert (List c)        xs = map (assert c) xs

pos :: (Num a, Ord a) => Contract a
pos = Pred (>0)

true :: Contract a
true = Pred (const True)
--- Proof:
--- 	assert true x
--- 	assert (Pred (const True)) x
--- 	if (const True) x then x else error
--- 	x 


preserves :: Eq b => (a -> b) -> Contract (a -> a)
preserves f = DFun true (\before -> Pred (\after -> f before == f after))

(-->) :: Contract a -> Contract b -> Contract (a -> b)
pre --> post = DFun pre (const post)

-- Index is preditace with type Contract ([a] -> Int -> a)
list = assert Index (!!) [1,2,3] 2


-- It's not the same. First defintion checkes that the sign is after the calculations equals the one before, not that it's positive.
preservesPos = preserves (>0)
preservesPos' = pos --> pos


allPos = List pos
allPos' = Pred (all (>0))
-- First definition using lists starts producing values and fails only on the first value not satisfying the condition.
-- For example the call
-- assert allPos [1,2,3,0]
-- >> [1,2,3,*** Exception: contract violation
-- produces first 3 values and then fails with and exception.
-- If we only need the first value eg.
-- take 1 $ assert allPos [1,2,3,0]
-- the function doesn't fail at all and produces the value 1.
-- The second definition using Pred always fails if there is an element not satisfying the assertion.

-- In general using List assertion is applied per element only when the element of the list is required (lazily).
-- This among other things means that we can use it for infinite lists.
-- Using the predicate on the other hands, we check the whole lists before proceding with function application, 
-- thus making it unable to be used on infite lists.



main = print $ list

