dup :: t -> (t, t)
dup x = (x, x)

-- Each application of dup increases the type signature by a factor of 2. 
-- Adding a few more composition will quickly lead to a slow computer ;)
f = dup . dup . dup . dup . dup . dup . dup . dup . dup . dup . dup . dup . dup . dup . dup 

-- An even "Better" way is to define the functions as follows.
-- Side note: It is a bad idea to check the type of f4 in console:)
f0 x = (x, x)
f1 :: t -> ((t, t), (t, t))
f1 = f0 . f0
f2
  :: a
     -> ((((a, a), (a, a)), ((a, a), (a, a))),
         (((a, a), (a, a)), ((a, a), (a, a))))
f2 = f1 . f1
f3 = f2 . f2
f4 = f3 . f3
